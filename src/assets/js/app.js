$(document).ready(function(){
	$('#nav-toggle').on('click', function(){
		if($('html').hasClass('offcanvas-active')) {
			$('html').removeClass('offcanvas-active');
		}
		else {
			$('html').addClass('offcanvas-active');
		}
	});
	$('#dismiss-offcanvas').on('click', function(){
		$('html').removeClass('offcanvas-active');
	});
	$('#off-canvas').find('.dropdown').each(function(){
		$(this).find('>a').on('click', function(e){
			var $this = $(this);
			e.preventDefault();
			if(!$this.closest('li').hasClass('active')) {
				$this.closest('li').addClass('active').siblings('li.dropdown.active').removeClass('active').find('ul').slideUp(300);
				$this.siblings('ul').slideDown(300);
			}
			else {
				window.location.href = $this.attr('href');
			}
		});
	});
	var headingBgResize = function() {
		var headingBg = $('#heading h1').hasClass('hidden');
		if(typeof headingBg !== typeof undefined && headingBg !== false) {
			var widthPercentage = $('#heading').width() / 1168;
			$('#heading').outerHeight(widthPercentage * 190);
		}
	};
	headingBgResize();
	$(window).resize(function(){
		if($(this).width() >= 768 && $('html').hasClass('offcanvas-active')) {
			$('html').removeClass('offcanvas-active');
		}
		headingBgResize();
	});
	$('#highlights').find('.section-body').slick({
		arrows: false,
		autoplay: true,
		autoplaySpeed: 7000,
		dots: true,
		infinite: true,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					autoplay: true,
					autoplaySpeed: 7000,
					dots: false
				}
			}
		]
	});

	//Search
	$('#search').find('select').on('change', function(){
		var $this = $(this);
		if($this.closest('.search-criteria').siblings('.search-criteria').find('select').val() !== "") {
			var selectedArea = $('#search').find('.search-criteria.areas-served').find('select').val(),
				selectedSpecialty = $('#search').find('.search-criteria.specialties').find('select').val();
			window.location.href = "/results?area=" + selectedArea + "&specialty=" + selectedSpecialty;
		}
	});
});