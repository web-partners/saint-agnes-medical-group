var gulp = require('gulp'),
	browserSync = require('browser-sync'),
	reload = browserSync.reload,
    concatvendor = require('gulp-concat-vendor'),
    sass = require('gulp-sass'),
    neat = require('node-neat').includePaths,
    autoprefixer = require('gulp-autoprefixer'),
    rename = require('gulp-rename'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    include = require('gulp-include'),
    uglify = require('gulp-uglify'),
    sprite = require('css-sprite').stream,
    gulpif = require('gulp-if'),
    notify = require('gulp-notify');

var paths = {
    html: './src/*.html',
    sass: './src/assets/sass/*.sass',
    js: './src/assets/js/app.js',
    img: './src/assets/img/{*.jpg,*.png,*.gif}',
    sprite: './src/assets/img/sprite/*.png',
    bower: './src/assets/vendor/*'
};

gulp.task('serve', function() {
    browserSync({
        server: './src'
    });
});

gulp.task('html', function() {
    return gulp.src(paths.html)
        .pipe(gulp.dest('dist'))
        .pipe(notify({ message: 'HTML task complete' }))
        .pipe(browserSync.reload({stream:true}));
});

gulp.task('bower', function() {
    return gulp.src(paths.bower)
        .pipe(concatvendor('vendor.js'))
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('src/assets/js'))
        .pipe(gulp.dest('dist/assets/js'))
        .pipe(gulp.dest('umbraco/scripts'))
        .pipe(browserSync.reload({stream:true}));
});

gulp.task('sass', function() {
    return gulp.src(paths.sass)
        .pipe(sass({
            indentedSyntax: true,
            includePaths: ['sass'].concat(neat)
        }))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    	.pipe(rename({suffix: '.min'}))
    	.pipe(minifycss())
        .pipe(gulp.dest('src/assets/css'))
    	.pipe(gulp.dest('dist/assets/css'))
    	.pipe(gulp.dest('umbraco/css'))
    	.pipe(notify({ message: 'CSS task complete' }))
    	.pipe(browserSync.reload({stream:true}));
});

gulp.task('sprite', function () {
    return gulp.src(paths.sprite)
        .pipe(sprite({
            name: 'sprite',
            style: '_sprite.sass',
            cssPath: '../img/',
            processor: 'sass'
        }))
        .pipe(gulpif('*.png', gulp.dest('./src/assets/img/'), gulp.dest('./src/assets/sass/')))
        .pipe(notify({ message: 'Sprite task complete' }))
        .pipe(browserSync.reload({stream:true}));
});

gulp.task('img', function() {
    return gulp.src(paths.img)
        .pipe(gulp.dest('dist/assets/img'))
        .pipe(gulp.dest('umbraco/img'))
        .pipe(browserSync.reload({stream:true}));
});

gulp.task('js', function() {
	return gulp.src(paths.js)
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('src/assets/js'))
	    .pipe(gulp.dest('dist/assets/js'))
	    .pipe(gulp.dest('umbraco/scripts'))
	    .pipe(notify({ message: 'JS task complete' }))
	    .pipe(browserSync.reload({stream:true}));
});

gulp.task('default', ['html', 'sass', 'sprite', 'bower', 'img', 'js', 'serve'], function () {
    gulp.watch(paths.html, ['html']);
	gulp.watch(paths.sass, ['sass']);
    gulp.watch(paths.img, ['img']);
    gulp.watch(paths.sprite, ['sprite']);
    gulp.watch(paths.js, ['js']);
    gulp.watch(paths.bower, ['bower']);
});