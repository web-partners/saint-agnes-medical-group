<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="true" CodeFile="DefaultSearch.ascx.cs" Inherits="Search" %>
<div>
	<div class="section-title">
		<h3>Search</h3>
	</div>
	<div class="section-body">
		<asp:TextBox runat="server" ID="txtSearch" placeholder="Search" aria-label="Search" />
		<asp:Button runat="server" ID="btnSubmit" Text="Submit" OnClick="SearchSubmitClick" />
	</div>
</div>
<div>
	<div class="section-body">
		<asp:DropDownList runat="server" ID="ddlSpecilaties" AppendDataBoundItems="true">
			<asp:ListItem Value="">Specialties</asp:ListItem>
		</asp:DropDownList>
	</div>
</div>