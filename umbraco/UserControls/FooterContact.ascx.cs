using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;
using System.Web.Configuration;


public partial class FooterContact : System.Web.UI.UserControl
{
	protected void Page_Load(object sender, EventArgs e)
	{
		if(!Page.IsPostBack) {
			MltFooterContact.ActiveViewIndex = 0;
		}
	}
	protected void ContactSubmitClick(object sender, EventArgs e)
	{
		Page.Validate("ContactFooter");
		if (Page.IsValid)
		{
			MltFooterContact.ActiveViewIndex = 1;
			if (String.IsNullOrEmpty(txtName.Text.Trim()))
			{
				try
				{
					string fullName = txtFirstName.Text.Trim() + ' ' + txtLastName.Text.Trim();
					MailMessage mail = new MailMessage();
					SmtpClient SmtpServer = new SmtpClient();
					mail.From = new MailAddress(txtEmail.Text.Trim(), fullName);
					mail.To.Add(new MailAddress("abarsotti@tivilon.com"));
					mail.Subject = "Saint Agnes Contact Form Submission";
					mail.Body = "<strong>Name: </strong>" + fullName + "<br/><strong>Message: </strong>" + txtMessage.Text.Trim();
					mail.IsBodyHtml = true;
					SmtpServer.EnableSsl = false;
					SmtpServer.Send(mail);
					litThankYou.Text = "<div class=\"success\">Thank you for contacting us! A representative will respond to you shortly.</div>";
				}
				catch (Exception ex)
				{
					litThankYou.Text = "<div class=\"error\">An error occurred. Please go back and try again.</div>";
				}
			}
			else
			{
				litThankYou.Text = "<div class=\"error\">An error occurred. Please go back and try again.</div>";
			}
		}
	}
}