using System;
using System.Web;
using System.Web.UI;
using umbraco;
using umbraco.NodeFactory;

public partial class Search : System.Web.UI.UserControl
{
	protected void Page_Load(object sender, EventArgs e)
	{
		if(!Page.IsPostBack) {
			PopulateSpecialtiesList();
		}
	}
	protected void PopulateSpecialtiesList(int specialtiesId)
	{
		Node specialties = new Node(specialtiesId);
		foreach(var specialty in specialties.Children.Where("nodeTypeAlias.Equals(\"Specialty\")"))
		{
			ddlSpecilaties.Items.Add(new ListItem(specialty.Name, specialty.Id));
		}
	}
	protected void SearchSubmitClick(object sender, EventArgs e)
	{
		string host = HttpContext.Current.Request.Url.Host;
		string redirectURL = "http://" + host + "/results?q=" + txtSearch.Text.Trim();
		if(!string.IsNullOrEmpty(ddlSpecilaties.SelectedValue))
		{
			redirectURL += "&specialty=" + ddlSpecilaties.SelectedValue;
		}
		Response.Redirect[redirectURL];
	}
}