<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="true" CodeFile="FooterContact.ascx.cs" Inherits="FooterContact" %>
<asp:UpdatePanel runat="server">
	<ContentTemplate>
		<asp:MultiView id="MltFooterContact" runat="server">
			<asp:View runat="server">
				<div class="form-left">
					<asp:TextBox ID="txtName" CssClass="hidden" runat="server" />
					<asp:TextBox ID="txtFirstName" runat="server" placeholder="First Name" aria-label="First Name" />
					<asp:RequiredFieldValidator ValidationGroup="ContactFooter" Display="dynamic" CssClass="form-error error" ControlToValidate="txtFirstName" runat="server" ErrorMessage="Please enter a first name." />
					<asp:TextBox ID="txtLastName" runat="server" placeholder="Last Name" aria-label="Last Name" />
					<asp:RequiredFieldValidator ValidationGroup="ContactFooter" Display="dynamic" CssClass="form-error error" ControlToValidate="txtLastName" runat="server" ErrorMessage="Please enter a last name." />
					<asp:TextBox ID="txtEmail" runat="server" placeholder="Email" aria-label="Email" />
					<asp:RegularExpressionValidator ValidationGroup="ContactFooter" Display="dynamic" CssClass="form-error error" ControlToValidate="txtEmail" ValidationExpression="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" ErrorMessage="Please enter a valid email." runat="server"/>
				</div>
				<div class="form-right">
					<asp:TextBox ID="txtMessage" runat="server" placeholder="Message" TextMode="MultiLine" />
					<asp:RequiredFieldValidator ValidationGroup="ContactFooter" Display="dynamic" CssClass="form-error error" ControlToValidate="txtMessage" runat="server" ErrorMessage="Please enter a message." />
					<asp:Button ID="ContactSubmitButton" ValidationGroup="ContactFooter" runat="server" OnClick="ContactSubmitClick" Text="Submit" CssClass="btn btn-primary" />
				</div>
			</asp:View>
			<asp:View runat="server">
				<asp:Literal runat="server" id="litThankYou" />
			</asp:View>
		</asp:MultiView>
	</ContentTemplate>
</asp:UpdatePanel>